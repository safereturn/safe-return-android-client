package safe.ret.android.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;

/**
 * Created by Eva on 03-Mar-17.
 */

public abstract class SafeReturnActivity extends AppCompatActivity {


    protected void goToActivity(Class activityClass){

        Intent loginIntent = new Intent(getApplicationContext(),activityClass);
        loginIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(loginIntent);
    }
}
