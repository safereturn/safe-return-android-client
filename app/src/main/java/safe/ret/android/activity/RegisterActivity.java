package safe.ret.android.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Toast;

import com.example.eva.safe.R;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import safe.ret.android.dto.UserDto;
import safe.ret.android.service.SafeReturnAPI;
import safe.ret.android.service.SafeReturnClient;
import safe.ret.android.utils.Globals;
import safe.ret.android.utils.Utils;

import static safe.ret.android.utils.Globals.INSTALLATION_MODE_TRACKED;
import static safe.ret.android.utils.Globals.PREFS_REG_TOKEN;
import static safe.ret.android.utils.Globals.PREFS_SAFERET_USRID;
import static safe.ret.android.utils.Globals.PREFS_TRACKED_ID;
import static safe.ret.android.utils.Globals.PREFS_USERNAME;


public class RegisterActivity extends SafeReturnActivity {

    /**
     * Registers a new user.Performs basic validation
     * On success triggers MainActivity.
     * //TODO Handle error responses from back end.
     */

    private EditText username;
    private EditText pass1;
    private EditText pass2;
    private EditText mobileField;

    private ProgressDialog progressDialog;
    private String installation_mode;
    private SafeReturnAPI apiService;




    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        apiService = SafeReturnClient.getClient().create(SafeReturnAPI.class);
        username = (EditText) findViewById(R.id.username);
        pass1 = (EditText) findViewById(R.id.pass1);
        pass2 = (EditText) findViewById(R.id.pass2);
        mobileField = (EditText) findViewById(R.id.mobile);
        progressDialog = new ProgressDialog(RegisterActivity.this);
    }

    public void onRadioButtonClicked(View view) {
        // Is the button now checked?
        boolean checked = ((RadioButton) view).isChecked();

        // Check which radio button was clicked
        switch(view.getId()) {
            case R.id.contact:
                if (checked)
                    installation_mode = Globals.INSTALLATION_MODE_CONTACT;
                    break;
            case R.id.tracked:
                if (checked)
                    installation_mode = Globals.INSTALLATION_MODE_TRACKED;
                    break;
        }
    }

    public void onRegisterClick(View view) {

        UserDto user  = new UserDto();
        user.setUsername(username.getText().toString());
        user.setPassword(pass1.getText().toString());
        user.setConfirmPass(pass2.getText().toString());
        user.setMobile(mobileField.getText().toString());
        user.setRegistrationId(Utils.getDefaults(PREFS_REG_TOKEN,getApplicationContext()));

        String validationError = validate(user,installation_mode);

        if(validationError!=null){
            Toast toastAlert = Toast.makeText(this, validationError, Toast.LENGTH_SHORT);
            toastAlert.show();
        }else{
            register(user);
        }
    }


    private String validate(UserDto user, String installation_mode){

        String validationError = null;

        if (installation_mode==null){

            validationError = "Please select installation mode";

        }else if(Utils.isNullOrEmpty(user.getUsername()) || Utils.isNullOrEmpty(user.getPassword())) {

            validationError = "Please fill the empty fields";

        }else if (!user.getPassword().equals(user.getConfirmPass())){

            validationError = "Please confirm password correctly";
        }
        return validationError;
    }



    private void register(UserDto user){

        progressDialog.setMessage("Please wait...");
        progressDialog.setCancelable(false);
        progressDialog.show();

        Call<UserDto> call;

        if(installation_mode.equals(INSTALLATION_MODE_TRACKED))
            call = apiService.createTrackedUser(user);
        else
            call = apiService.createContactPerson(user);

        Log.d("URL",call.request().url().toString());

        call.enqueue(new Callback<UserDto>() {
            @Override
            public void onResponse(Call<UserDto> call, Response<UserDto> response) {
                Toast.makeText(getApplicationContext(), "Registration was successful!", Toast.LENGTH_SHORT).show();
                Utils.setDefaults(PREFS_USERNAME, response.body().getUsername(), getBaseContext());
                Utils.setDefaults(PREFS_SAFERET_USRID, response.body().getId().toString(), getBaseContext());
                Utils.setDefaults(Globals.PREFS_INST_MODE, installation_mode, getBaseContext());
                if(INSTALLATION_MODE_TRACKED.equals( installation_mode)){
                   Utils.setDefaults(PREFS_TRACKED_ID, response.body().getId().toString(), getBaseContext());
                }

                progressDialog.dismiss();
                goToActivity(MainActivity.class);
            }
            @Override
            public void onFailure(Call<UserDto> call, Throwable t) {
                Log.d("ERR", "ERR", t); progressDialog.dismiss();
            }
        });
    }

}
