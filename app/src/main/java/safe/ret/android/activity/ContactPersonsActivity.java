
package safe.ret.android.activity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.eva.safe.R;

import java.io.IOException;
import java.lang.annotation.Annotation;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Converter;
import retrofit2.Response;
import safe.ret.android.SafeRetApplication;
import safe.ret.android.dto.AddContactRequest;
import safe.ret.android.dto.Contact;
import safe.ret.android.dto.SafeReturnMessage;
import safe.ret.android.service.SafeReturnAPI;
import safe.ret.android.service.SafeReturnClient;
import safe.ret.android.utils.Utils;

import static safe.ret.android.utils.Globals.PREFS_TRACKED_ID;

public class ContactPersonsActivity extends SafeReturnActivity {

    private SafeReturnAPI apiService;
    private TextView firstNameField;
    private TextView mobileField;
    private Spinner relationTypeSpinner;

    private String relationType;


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_persons);
        apiService = SafeReturnClient.getClient().create(SafeReturnAPI.class);

        firstNameField = (TextView) findViewById(R.id.firstName);
        mobileField = (TextView) findViewById(R.id.mobile);
        relationTypeSpinner = (Spinner) findViewById(R.id.relation_spinner);

        this.setUpSpinner();

        Contact contact = (Contact) getIntent().getSerializableExtra("contact");
        if (contact != null) {
            firstNameField.setText(contact.getDisplayName());
            mobileField.setText(contact.getMobile());
        }
    }



    public void createContactPerson(View view) {

        AddContactRequest addContactRequest = new AddContactRequest(mobileField.getText().toString(),
                Utils.getDefaults(PREFS_TRACKED_ID, SafeRetApplication.getAppContext()),
                firstNameField.getText().toString(),
                relationType);

        Call<SafeReturnMessage> call = apiService.addContact(addContactRequest);
        call.enqueue(new Callback<SafeReturnMessage>() {

            @Override
            public void onResponse(Call<SafeReturnMessage> call, Response<SafeReturnMessage> response ) {

                if (response != null && !response.isSuccessful() && response.errorBody() != null) {

                    handleErrorResponse(response.errorBody());
                }
                else if (response.body()!=null){

                    Toast.makeText(SafeRetApplication.getAppContext(), response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    goToActivity(MainActivity.class);
                }
            }

            @Override
            public void onFailure(Call<SafeReturnMessage> call, Throwable t) {
                Log.d("ERR", "ERR", t);
            }
        });
    }


    private void setUpSpinner() {

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.relation_types_array, android.R.layout.simple_spinner_item);

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        relationTypeSpinner.setAdapter(adapter);
        relationTypeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
              //pos >0 because,spinner description is used in position 0
              if (position > 0) {
                  relationType = (String) parent.getItemAtPosition(position);
              } else relationType = "";

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
              relationType = "";
            }
        });
    }

    private void handleErrorResponse(ResponseBody errorBody){
        Converter<ResponseBody, SafeReturnMessage> errorConverter =
                SafeReturnClient.getClient().responseBodyConverter(SafeReturnMessage.class, new Annotation[0]);
        try {

            SafeReturnMessage error = errorConverter.convert(errorBody);
            Toast.makeText(SafeRetApplication.getAppContext(), error.getMessage(), Toast.LENGTH_SHORT).show();

        } catch (IOException e) {
            e.printStackTrace();
        }
        //DO ERROR HANDLING HERE
        return;
    }

}
