package safe.ret.android.activity;

import android.content.Context;
import android.content.Intent;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;

import com.example.eva.safe.R;

import safe.ret.android.SafeRetApplication;
import safe.ret.android.fragment.ContactsFragment;
import safe.ret.android.fragment.FragmentOne;
import safe.ret.android.fragment.MapFragment;
import safe.ret.android.fragment.OptionsFragment;
import safe.ret.android.service.LocationService;
import safe.ret.android.adapter.ViewPagerAdapter;
import safe.ret.android.service.fcm.TestLocationService;
import safe.ret.android.utils.Globals;
import safe.ret.android.utils.Utils;

import static safe.ret.android.utils.Globals.PREFS_SAFERET_USRID;
import static safe.ret.android.utils.Globals.TAB_CONTACTS;
import static safe.ret.android.utils.Globals.TAB_HOME;
import static safe.ret.android.utils.Globals.TAB_MAP;
import static safe.ret.android.utils.Globals.TAB_OPTIONS;

/**
 *  Application entry point
 *  Displays registration screen if app is used for the first time
 *  If it's a registered client the tab layout will be displayed depending on
 *  user type (tracked/contact)
 */

public class MainActivity  extends AppCompatActivity  {

    private TabLayout tabLayout;
    private ViewPager viewPager;
    Intent intent;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //if user is not registered
        if(Utils.getDefaults(PREFS_SAFERET_USRID, SafeRetApplication.getAppContext())==null){
            Intent intent = new Intent(this,RegisterActivity.class);
            startActivity(intent);
        }else{

            setContentView(R.layout.activity_tab);
            viewPager = (ViewPager) findViewById(R.id.viewpager);

            //registered as tracked user
            if  (Utils.getDefaults(Globals.PREFS_INST_MODE,SafeRetApplication.getAppContext()).equals(Globals.INSTALLATION_MODE_TRACKED)){

                setUpTabsForTrackedMode(viewPager);
                tabLayout = (TabLayout) findViewById(R.id.tabs);
                tabLayout.setupWithViewPager(viewPager);
                //TODO enable data
                WifiManager wifi = (WifiManager) getSystemService(Context.WIFI_SERVICE);
                wifi.setWifiEnabled(true);

                intent = new Intent(this, TestLocationService.class);
                startService(intent);

            //registered as contact person
            }else if  (Utils.getDefaults(Globals.PREFS_INST_MODE,SafeRetApplication.getAppContext()).equals(Globals.INSTALLATION_MODE_CONTACT)){

                setUpTabsForContactMode(viewPager);
                tabLayout = (TabLayout) findViewById(R.id.tabs);
                tabLayout.setupWithViewPager(viewPager);

            }
        }
    }

    private void setUpTabsForContactMode(ViewPager viewPager){

        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new FragmentOne(), TAB_HOME);
        adapter.addFragment(new OptionsFragment(), TAB_OPTIONS);
        adapter.addFragment(new MapFragment(), TAB_MAP);
        viewPager.setAdapter(adapter);
        //solves issue http://stackoverflow.com/questions/9610495/oncreate-called-for-two-tabs-each-time-one-tab-is-selected
        //number of tabs - 1 ,starts from 0
        viewPager.setOffscreenPageLimit(2);

    }


    private void setUpTabsForTrackedMode(ViewPager viewPager){

        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new FragmentOne(), TAB_HOME);
        adapter.addFragment(new OptionsFragment(), TAB_OPTIONS);
        adapter.addFragment(new ContactsFragment(), TAB_CONTACTS);
        adapter.addFragment(new MapFragment(), TAB_MAP);
        viewPager.setAdapter(adapter);
        //solves issue http://stackoverflow.com/questions/9610495/oncreate-called-for-two-tabs-each-time-one-tab-is-selected
        viewPager.setOffscreenPageLimit(3);

    }

}
