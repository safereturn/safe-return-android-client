package safe.ret.android.activity;

import android.content.ContentResolver;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.example.eva.safe.R;

import java.util.ArrayList;
import java.util.List;

import safe.ret.android.adapter.CustomContactAdapter;
import safe.ret.android.dto.Contact;

/**
 * Activity used to read contacts from the device.
 *
 */

public class ContactPicker extends AppCompatActivity {

    private CustomContactAdapter customAdapter;

    private ListView lvContactPicker;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.contact_picker);
        lvContactPicker = (ListView) findViewById(R.id.contact_picker_list_view);
        customAdapter = new CustomContactAdapter(getApplicationContext(), readContactsFromDevice());
        lvContactPicker.setAdapter(customAdapter);
        lvContactPicker.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                final Contact c = customAdapter.getItem(position);
                new AlertDialog.Builder(ContactPicker.this)
                        .setTitle("Add contact")
                        .setMessage("Add " + c.getDisplayName() + " as contact person?")
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

                            public void onClick(DialogInterface dialog, int which) {

                                Intent newIntent = new Intent(getApplicationContext(), ContactPersonsActivity.class);
                                newIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                Contact  person = new Contact(c.getMobile() , c.getDisplayName());
                                newIntent.putExtra("contact", person);
                                startActivity(newIntent);
                            }
                        })
                        .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                // do nothing
                            }
                        })
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .show();
            }
        });
    }


    private List<Contact> readContactsFromDevice() {

        List<Contact> contacts = new ArrayList<>();
        ContentResolver cr = getContentResolver();
        Cursor cur = cr.query(ContactsContract.Contacts.CONTENT_URI, null, null, null, "upper(" + ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME + ") ASC");
        if (cur.getCount() > 0) {
            while (cur.moveToNext()) {
                String id = cur.getString(cur.getColumnIndex(ContactsContract.Contacts._ID));
                String name = cur.getString(cur.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
                if (Integer.parseInt(cur.getString(cur.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER))) > 0) {

                    //get the phone number
                    Cursor pCur = cr.query(
                            ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,
                            ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?",
                            new String[]{id}, null);
                    while (pCur.moveToNext()) {
                        String phoneNo = pCur.getString(pCur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                        contacts.add(new Contact(phoneNo, name));
                    }
                    pCur.close();
                }
            }
        }
        cur.close();
        return contacts;
    }


}
