package safe.ret.android.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.eva.safe.R;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import safe.ret.android.SafeRetApplication;
import safe.ret.android.dto.OptionDto;
import safe.ret.android.service.SafeReturnAPI;
import safe.ret.android.service.SafeReturnClient;
import safe.ret.android.utils.Utils;

import static safe.ret.android.utils.Globals.DEFAULT_DISTANCE_OPTION;
import static safe.ret.android.utils.Globals.DEFAULT_TIME_OPTION;
import static safe.ret.android.utils.Globals.PREFS_DISTANCE_OPTION;
import static safe.ret.android.utils.Globals.PREFS_TIME_OPTION;
import static safe.ret.android.utils.Globals.PREFS_TRACKED_ID;

public class OptionsFragment extends Fragment {

    private Button applyBtn;
    private TextView distanceField;
    private TextView timeField;
    private SafeReturnAPI apiService;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        apiService = SafeReturnClient.getClient().create(SafeReturnAPI.class);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_options, container, false);
        distanceField = (TextView) view.findViewById(R.id.distanceField);
        timeField = (TextView) view.findViewById(R.id.timeField);

        OptionDto options = getOptions();
        timeField.setText(String.valueOf( options.getTime() ));
        distanceField.setText(String.valueOf( options.getDistance() ));

        applyBtn = (Button) view.findViewById(R.id.applyBtn);
        applyBtn.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                OptionDto options = new OptionDto();
                options.setDistance(Double.valueOf( distanceField.getText().toString() ));
                options.setTime(Integer.valueOf( timeField.getText().toString() ));
                options.setTrackedUserId(Long.valueOf(Utils.getDefaults(PREFS_TRACKED_ID, SafeRetApplication.getAppContext())));
                storeOptions(options);

            }
        });
        return view;
    }

    private OptionDto getOptions(){

        OptionDto options = new OptionDto();
        options.setDistance(Double.valueOf(Utils.getDefaults(PREFS_DISTANCE_OPTION, DEFAULT_DISTANCE_OPTION, SafeRetApplication.getAppContext())));
        options.setTime(Integer.valueOf(Utils.getDefaults(PREFS_TIME_OPTION, DEFAULT_TIME_OPTION, SafeRetApplication.getAppContext())));
        return options;
    }

    /**
     * Informs server side for the updated values
     * On success stores them locally as shared preferences.
     *
     * @param options
     */
    private void storeOptions(OptionDto options){

        Call<OptionDto> call = apiService.storeOptions(options);
        call.enqueue(new Callback<OptionDto>() {
            @Override
            public void onResponse(Call<OptionDto> call, Response<OptionDto> response) {
                OptionDto options = response.body();
                Utils.setDefaults(PREFS_DISTANCE_OPTION,String.valueOf( options.getDistance() ),SafeRetApplication.getAppContext());
                Utils.setDefaults(PREFS_TIME_OPTION,String.valueOf( options.getTime() ),SafeRetApplication.getAppContext());
                Toast.makeText(SafeRetApplication.getAppContext(), "Changes applied", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<OptionDto> call, Throwable t) {
                Log.d("Safe return", "Store Options", t);
            }
        });
    }


}
