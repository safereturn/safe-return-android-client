package safe.ret.android.fragment;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.eva.safe.R;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import safe.ret.android.SafeRetApplication;
import safe.ret.android.dto.Location;
import safe.ret.android.listener.AddMarkerListener;
import safe.ret.android.listener.RemoveMarkerListener;
import safe.ret.android.service.SafeReturnAPI;
import safe.ret.android.service.SafeReturnClient;
import safe.ret.android.utils.Globals;
import safe.ret.android.utils.Utils;

import static safe.ret.android.utils.Globals.PREFS_SAFE_LOCATIONS_SET;
import static safe.ret.android.utils.Globals.PREFS_TRACKED_ID;

public class MapFragment extends Fragment {

    private SafeReturnAPI apiService;
    private GoogleMap googleMap;
    private List<LatLng> safeLocations;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.apiService = SafeReturnClient.getClient().create(SafeReturnAPI.class);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_map, container, false);
        MapView mMapView = (MapView) rootView.findViewById(R.id.mapView);
        mMapView.onCreate(savedInstanceState);
        mMapView.onResume(); // needed to get the map to display immediately
        mMapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap mMap) {
                googleMap = mMap;
                googleMap.setMyLocationEnabled(true);
                if (isSafeLocationConfigured()) {
                    safeLocations = retrieveSafeLocations();
                    addSafeLocationMarkers(safeLocations);
                }
                getLastKnownLocation();
                LatLng targetLocation = getTargetLocation();
                CameraPosition cameraPosition = new CameraPosition.Builder().target(targetLocation).zoom(12).build();
                googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
                googleMap.setOnMapClickListener(new AddMarkerListener(googleMap, getActivity()));
                googleMap.setOnMarkerClickListener(new RemoveMarkerListener(googleMap, getActivity()));

            }
        });

        return rootView;
    }


    private List<LatLng> retrieveSafeLocations() {

        final List<LatLng> locations = new ArrayList<>();
        Call<List<Location>> call = apiService.getSafeLocations(Utils.getDefaults(PREFS_TRACKED_ID, SafeRetApplication.getAppContext()),true);
        call.enqueue(new Callback<List<Location>>() {
            @Override
            public void onResponse(Call<List<Location>> call, Response<List<Location>> response) {
                List<Location> lst = response.body();
                if (lst != null) {
                    for (Location l : lst) {
                        locations.add(new LatLng(l.getLatitude()    , l.getLongitude()));
                    }
                    addSafeLocationMarkers(locations);
                }
            }

            @Override
            public void onFailure(Call<List<Location>> call, Throwable t) {
                Log.d("ERR", "ERR", t);
            }
        });
        return locations;
    }


    private void getLastKnownLocation() {
        Call<Location> call = apiService.getLastKnownLocation(Utils.getDefaults(PREFS_TRACKED_ID, SafeRetApplication.getAppContext()));
        call.enqueue(new Callback<Location>() {
            @Override
            public void onResponse(Call<Location> call, Response<Location> response) {
                Location location = response.body();
                if (location != null) {
                    BitmapDescriptor MARKER_ICON = BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_VIOLET);
                    googleMap.addMarker(new MarkerOptions().icon(MARKER_ICON).position(new LatLng(location.getLatitude(), location.getLongitude()))
                            .title("Marker Title")
                            .snippet("Marker Description"));
                }
            }

            @Override
            public void onFailure(Call<Location> call, Throwable t) {
                Log.d("ERR", "ERR", t);
            }
        });
    }


    private void addSafeLocationMarkers(List<LatLng> safeLocations) {
        googleMap.clear();
        BitmapDescriptor MARKER_ICON = BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE);
        for (LatLng latlng : safeLocations) {
            googleMap.addMarker(new MarkerOptions().icon(MARKER_ICON).position(latlng).title("Marker Title").snippet("Marker Description"));
        }
    }

    private LatLng getTargetLocation() {
        String lat = Utils.getDefaults("last_known_lat", SafeRetApplication.getAppContext());
        String lng = Utils.getDefaults("last_known_lng", SafeRetApplication.getAppContext());
        if (lat != null && lng != null) {
            return new LatLng(Double.valueOf(lat), Double.valueOf(lng));
        } else return new LatLng(30, 30);
    }


    private boolean isSafeLocationConfigured() {
        return Globals.TRUE.equals(Utils.getDefaults(PREFS_SAFE_LOCATIONS_SET, getContext()));
    }


}
