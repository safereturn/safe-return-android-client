package safe.ret.android.fragment;

import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.eva.safe.R;
import com.google.firebase.FirebaseApp;
import com.google.firebase.iid.FirebaseInstanceId;

import java.io.IOException;

import safe.ret.android.SafeRetApplication;
import safe.ret.android.utils.Utils;

import static safe.ret.android.utils.Globals.PREFS_REG_TOKEN;


public class FragmentOne extends Fragment {

    public FragmentOne() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        Log.d("fragment_one", "onCreateView");

        View view = inflater.inflate(R.layout.fragment_one, container, false);

        String username = Utils.getDefaults("user", getContext());
        String tracked = Utils.getDefaults("tracked", getContext());
        Log.d("Restored", username == null ? "nada" : username);
        Log.d("Tracked Person", tracked == null ? "nada" : tracked);

      /*  TextView tvName = (TextView) view.findViewById(R.id.usertv);
        tvName.setText(username);
*/

        TextView reg = (TextView) view.findViewById(R.id.reg);
        reg.setText( FirebaseInstanceId.getInstance(FirebaseApp.getInstance()).getToken() );

        reg.setText( FirebaseInstanceId.getInstance(FirebaseApp.getInstance()).getId() );

        TextView prefs = (TextView) view.findViewById(R.id.prefs);

       SharedPreferences s =  PreferenceManager.getDefaultSharedPreferences(SafeRetApplication.getAppContext());
        java.util.Map<String, ?> all =  s.getAll();

        StringBuilder sb = new StringBuilder();
        for (java.util.Map.Entry entry : all.entrySet()) {
            if (entry.getKey().equals(PREFS_REG_TOKEN))
                continue;
             sb.append(entry.getKey() + ", " + entry.getValue());
            sb.append("\n");

        }
    System.out.println(sb.toString());
        prefs.setText(sb.toString());
        Button bt = (Button) view.findViewById(R.id.btn2);
        bt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteToken();
            }
        });

        Button bt3 = (Button) view.findViewById(R.id.btn3);
        bt3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                PreferenceManager.getDefaultSharedPreferences(SafeRetApplication.getAppContext())
                        .edit().clear().commit();
            }
        });

        return view;

    }


    private void deleteToken() {

        //Execution  in the main thread causes to IOException
        new DeleteTokenTask().execute();

    }

    private class DeleteTokenTask extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... voids) {
            try {


                FirebaseInstanceId.getInstance().deleteInstanceId();
                String newIID = FirebaseInstanceId.getInstance(FirebaseApp.getInstance()).getId();
                String newToken = FirebaseInstanceId.getInstance(FirebaseApp.getInstance()).getToken();
                System.out.println(newToken);
            } catch (IOException e) {
                Log.d("DeleteTokenTask", "Exception deleting token", e);
            }
            return null;
        }
    }
}
