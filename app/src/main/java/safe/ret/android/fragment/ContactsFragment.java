package safe.ret.android.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;

import com.example.eva.safe.R;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import safe.ret.android.SafeRetApplication;
import safe.ret.android.activity.ContactPersonsActivity;
import safe.ret.android.activity.ContactPicker;
import safe.ret.android.adapter.CustomContactAdapter;
import safe.ret.android.dto.Contact;
import safe.ret.android.service.SafeReturnAPI;
import safe.ret.android.service.SafeReturnClient;
import safe.ret.android.utils.Utils;

import static safe.ret.android.utils.Globals.EXTRAS_CONTACT;
import static safe.ret.android.utils.Globals.PREFS_TRACKED_ID;


public class ContactsFragment extends Fragment {

    private Button contactPickerBtn;
    private ListView contactsView;
    private CustomContactAdapter customAdapter;
    private SafeReturnAPI apiService;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        apiService = SafeReturnClient.getClient().create(SafeReturnAPI.class);
        customAdapter = new CustomContactAdapter(SafeRetApplication.getAppContext(), new ArrayList<Contact>());
        getContactPersons(Utils.getDefaults(PREFS_TRACKED_ID, SafeRetApplication.getAppContext()));
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_contacts, container, false);

        contactPickerBtn = (Button) view.findViewById(R.id.button2);
        contactPickerBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), ContactPicker.class);
                startActivity(intent);
            }
        });

        //list view to hold stored contact persons
        contactsView = (ListView) view.findViewById(R.id.contactList);
        contactsView.setAdapter(customAdapter);
        contactsView.setOnItemClickListener(new AdapterView.OnItemClickListener(){
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Contact person =  (Contact)parent.getItemAtPosition(position);
                Intent newIntent = new Intent(getContext(), ContactPersonsActivity.class);
                newIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                newIntent.putExtra(EXTRAS_CONTACT, person);
                startActivity(newIntent);

            }
        });
        return view;
    }

    /**
     *
     * Performs backend call to get contact persons for the
     * provided tracked user id.
     *
     */
    private void getContactPersons(String trackedUserID) {

        Call<List<Contact>> call = apiService.getContacts(trackedUserID);
        call.enqueue(new Callback<List<Contact>>() {

            @Override
            public void onResponse(Call<List<Contact>> call, Response<List<Contact>> response) {

                List<Contact> contacts = response.body();
                if(contacts!=null) {
                    //refresh list view
                    customAdapter.updateContacts(contacts);
                    customAdapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onFailure(Call<List<Contact>> call, Throwable t) {
                Log.d("ERR", "ERR", t);
            }
        });

    }



}
