package safe.ret.android.dto;

/**
 * Created by Eva on 19-Feb-17.
 */

public class AddContactRequest {

    private String contactMobile;
    private String trackedUserId;
    private String displayName;
    //private String relationType;

    public AddContactRequest(String contactMobile, String trackedUserId,String displayName,String relationType) {
        this.contactMobile = contactMobile;
        this.trackedUserId = trackedUserId;
        this.displayName = displayName;
    //   this.relationType = relationType;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getContactMobile() {
        return contactMobile;
    }

    public void setContactMobile(String contactMobile) {
        this.contactMobile = contactMobile;
    }

    public String getTrackedUserId() {
        return trackedUserId;
    }

    public void setTrackedUserId(String trackedUserId) {
        this.trackedUserId = trackedUserId;
    }

   /* public String getRelationType() {
        return relationType;
    }

    public void setRelationType(String relationType) {
        this.relationType = relationType;
    }*/
}
