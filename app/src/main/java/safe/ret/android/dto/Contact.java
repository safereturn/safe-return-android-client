package safe.ret.android.dto;

import java.io.Serializable;

/**
 * Created by Eva on 19-Feb-17.
 */

public class Contact implements Serializable {

    private String mobile;
    private String displayName;


    public Contact(String mobile, String displayName) {
        super();
        this.mobile = mobile;
        this.displayName = displayName;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    @Override
    public String toString() {
        return this.displayName + ", " + this.mobile;
    }
}
