package safe.ret.android.dto;

import java.io.Serializable;
import java.util.Date;


    public class Location   implements Serializable {

        private double latitude;
        private double longitude;
        private String trackedUserId;
         private Date date;
        private boolean home;



        public Location () {
            super();
            // TODO Auto-generated constructor stub
        }



        public double getLatitude() {
            return latitude;
        }

        public void setLatitude(double latitude) {
            this.latitude = latitude;
        }

        public double getLongitude() {
            return longitude;
        }

        public void setLongitude(double longitude) {
            this.longitude = longitude;
        }

        public String getTrackedUserId() {
            return trackedUserId;
        }

        public void setTrackedUserId(String trackedUserId) {
            this.trackedUserId = trackedUserId;
        }

        public Date getDate() {
            return date;
        }

        public void setDate(Date date) {
            this.date = date;
        }

        public boolean isHome() {
            return home;
        }

        public void setHome(boolean home) {
            this.home = home;
        }


    }