
package safe.ret.android.adapter;


import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.eva.safe.R;

import java.util.List;

import safe.ret.android.dto.Contact;

import static android.R.layout.simple_list_item_2;

public class CustomContactAdapter extends BaseAdapter {

    private Context mContext;
    private List<Contact> contactList;
    public CustomContactAdapter(Context context, List<Contact> contactList) {
        mContext = context;
        this.contactList = contactList;


    }
    @Override
    public int getCount() {
        return contactList.size();
    }
    @Override
    public Contact getItem(int position) {

        return contactList.get(position);
    }
    @Override
    public long getItemId(int position) {
        return 0;
    }


  @Override
    public View getView(int position, View convertView, ViewGroup parent) {

      // Get the data item for this position

      Contact user = getItem(position);

      // Check if an existing view is being reused, otherwise inflate the view

      if (convertView == null) {

          convertView = LayoutInflater.from(mContext) .inflate(simple_list_item_2, parent, false);

      }


      TextView  textView = (TextView)convertView.findViewById(android.R.id.text1);
      textView.setText(user.getDisplayName());
      textView.setTextColor(Color.DKGRAY);

      TextView  textView2 = (TextView)convertView.findViewById(android.R.id.text2);
      textView2.setText(user.getMobile());
      return convertView;
    }




    public void updateContacts(List<Contact> contactList) {
        this.contactList= contactList;
    }
}

