package safe.ret.android.utils;

import org.json.JSONException;
import org.json.JSONObject;

import safe.ret.android.SafeRetApplication;

import static safe.ret.android.utils.Globals.PREFS_TRACKED_ID;

/**
 * Created by Eva on 17-Feb-17.
 */

public class NotificationHelper {


    public static final String TRACKED_ID_PARAM = "tracked_user_id";
    public static final String TRACKED_USERNAME_PARAM = "tracked_user_name";


    public String buildNotificationText(String code,String body){


        String text = null;
        if (Globals.CONTACT_ADDED.equals(code)) {
            try {

                JSONObject json = new JSONObject(body);
                text = json.getString(TRACKED_USERNAME_PARAM) + " added you as contact person";


            } catch (JSONException e) {
                e.printStackTrace();
            }
        }else if (Globals.LOCATION_INFO.equals(code)){
            try {
                JSONObject json = new JSONObject(body);
                //TODO PROCESS LOCATION
                text = json.getString("msg");

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return text;
    }


}
