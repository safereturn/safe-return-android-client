package safe.ret.android.utils;

import java.text.SimpleDateFormat;

/**
 * Created by Eva on 06-Feb-17.
 */

public final class Globals {

    public static final String DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSSZ";

    //private  static final String REST_BASE_URL = "https://blooming-sands-90821.herokuapp.com/";
    private static final String REST_BASE_URL = "http://192.168.1.3:8080/";

    public static final String REGISTER_TRACKED_USER_URL = REST_BASE_URL + "tracked-user";
    public static final String REGISTER_CONTACT_USER_URL = REST_BASE_URL + "contact-person";
    public static final String ADD_CONTACT_URL = REST_BASE_URL + "tracked-user/contact";
    public static final String GET_CONTACTS_URL = REST_BASE_URL + "tracked-user/get-contacts";

    public static final String PUT_LOCATION_URL = REST_BASE_URL + "location/";
    public static final String CHECK_LOCATION_URL = REST_BASE_URL + "location/check-location";
    public static final String GET_LOCATIONS_URL = REST_BASE_URL + "location/";
    public static final String GET_LAST_KNOWN_LOCATION_URL = REST_BASE_URL + "location/last-location";

    public static final String POST_OPTIONS_URL = REST_BASE_URL + "options";
    public static final String UPDATE_TOKEN_URL = REST_BASE_URL + "user/updateToken";


    public static final String TRUE = "true";
    public static final String FALSE = "false";

    //SHARED PREFERENCES KEYS
    public static final String PREFS_SAFERET_USRID =  "user_id";
    public static final String PREFS_USERNAME = "user";
    public static final String PREFS_TRACKED_ID = "tracked_person_id";
    public static final String PREFS_TIME_OPTION = "time_option";
    public static final String PREFS_DISTANCE_OPTION = "distance_option";
    public static final String PREFS_SAFE_LOCATIONS_SET = "locations_set";
    public static final String PREFS_REG_TOKEN = "reg_token";
    public static final String PREFS_INST_MODE = "installation_mode";

    //SHARED PREFERENCES DEFAULT VALUES
    public static final String INSTALLATION_MODE_TRACKED = "TRACKED";
    public static final String INSTALLATION_MODE_CONTACT = "CONTACT";
    public static final String DEFAULT_DISTANCE_OPTION = "100";
    public static final String DEFAULT_TIME_OPTION = "5";


    public static final String TAB_HOME = "HOME";
    public static final String TAB_CONTACTS = "CONTACTS";
    public static final String TAB_MAP = "MAP";
    public static final String TAB_OPTIONS = "OPTIONS";

    //ATTRIBUTES FOR PUSH NOTIFICATIONS
    public static final String NOTIFICATION_CODE = "code";
    public static final String NOTIFICATION_TITLE = "title";
    public static final String NOTIFICATION_BODY = "body";


    //SAFE RETURN CODES FOR NOTIFICATIONS
    public static final String CONTACT_ADDED = "CTAD";
    public static final String LOCATION_INFO = "LOCINF";

    //REST QUERY PARAMETERS
    public static final String TRACKED_USER_ID_PARAM = "tracked-user-id";
    public static final String LOCATION_SAFE_PARAM = "safe";

    //EXTRAS
    public static final String EXTRAS_CONTACT = "contact";

}
