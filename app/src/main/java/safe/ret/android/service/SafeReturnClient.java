package safe.ret.android.service;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.lang.annotation.Annotation;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;
import safe.ret.android.dto.SafeReturnMessage;
import safe.ret.android.utils.Globals;

/**
 * Created by Eva on 18-Feb-17.
 */

public class SafeReturnClient {
    public static final String BASE_URL =  "https://blooming-sands-90821.herokuapp.com/";
    //public static final String BASE_URL = "http://localhost:8080/";
    private static Retrofit retrofit = null;


    public static Retrofit getClient() {

        if (retrofit==null) {

            Gson gson = new GsonBuilder()
                    .setDateFormat(Globals.DATE_FORMAT)
                    .create();


            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)

                   // .addConverterFactory(GsonConverterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create(gson))

                    .addConverterFactory(ScalarsConverterFactory.create())
                    .build();
        }
         return retrofit;
    }
}
