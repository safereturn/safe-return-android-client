package safe.ret.android.service.fcm;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;

import com.example.eva.safe.R;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import org.json.JSONException;
import org.json.JSONObject;

import safe.ret.android.SafeRetApplication;
import safe.ret.android.activity.MainActivity;
import safe.ret.android.utils.Globals;
import safe.ret.android.utils.NotificationHelper;
import safe.ret.android.utils.Utils;

import static safe.ret.android.utils.Globals.NOTIFICATION_BODY;
import static safe.ret.android.utils.Globals.NOTIFICATION_CODE;
import static safe.ret.android.utils.Globals.NOTIFICATION_TITLE;
import static safe.ret.android.utils.Globals.PREFS_TRACKED_ID;

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private NotificationHelper nHelper = new NotificationHelper();

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {

        super.onMessageReceived(remoteMessage);

        java.util.Map<String, String> data = remoteMessage.getData();

        String body = data.get(NOTIFICATION_BODY);
        String code = data.get(NOTIFICATION_CODE);
        String title = data.get(NOTIFICATION_TITLE);

        if (Globals.CONTACT_ADDED.equals(code)) {

            storePrefs(body);

        }
        body = nHelper.buildNotificationText(code, body);
        display(title, body, remoteMessage.getMessageId());

    }


    private void display(String title, String text, String id) {
        Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(R.drawable.common_google_signin_btn_icon_light)
                        .setContentTitle(title)
                        .setContentText(text);
        // Creates an explicit intent for an Activity in your app
        Intent resultIntent = new Intent(this, MainActivity.class);

        // The stack builder object will contain an artificial back stack for the
        // started Activity.
        // This ensures that navigating backward from the Activity leads out of
        // your application to the Home screen.
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
        // Adds the back stack for the Intent (but not the Intent itself)
        stackBuilder.addParentStack(MainActivity.class);
        // Adds the Intent that starts the Activity to the top of the stack
        stackBuilder.addNextIntent(resultIntent);
        PendingIntent resultPendingIntent =
                stackBuilder.getPendingIntent(
                        0,
                        PendingIntent.FLAG_UPDATE_CURRENT
                );
        mBuilder.setSound(alarmSound);

        mBuilder.setContentIntent(resultPendingIntent);
        NotificationManager mNotificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        // mId allows you to update the notification later on.
//        int mId = Integer.valueOf(id);
        mNotificationManager.notify(0, mBuilder.build());
        // mNotificationManager.notify();
    }

    private void storePrefs(String body){
        try {
            JSONObject jsonObject = new JSONObject(body);
            Utils.setDefaults(PREFS_TRACKED_ID,jsonObject.getString(NotificationHelper.TRACKED_ID_PARAM), SafeRetApplication.getAppContext());
           System.out.println( Utils.getDefaults(PREFS_TRACKED_ID,  SafeRetApplication.getAppContext()) );
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
