package safe.ret.android.service.fcm;

import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

import java.io.IOException;

import retrofit2.Call;
import safe.ret.android.SafeRetApplication;
import safe.ret.android.dto.UpdateTokenRequest;
import safe.ret.android.service.SafeReturnAPI;
import safe.ret.android.service.SafeReturnClient;
import safe.ret.android.utils.Globals;
import safe.ret.android.utils.Utils;

import static android.content.ContentValues.TAG;
import static safe.ret.android.utils.Globals.PREFS_REG_TOKEN;

/**
 * Created by Eva on 08-Feb-17.
 */

public class MyFirebaseInstanceIDService extends FirebaseInstanceIdService {

    private SafeReturnAPI apiService;

    public MyFirebaseInstanceIDService() {
        super();
        apiService = SafeReturnClient.getClient().create(SafeReturnAPI.class);
    }

    @Override
    public void onTokenRefresh() {
        // Get updated InstanceID token.
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.d(TAG, "Refreshed token: " + refreshedToken);

        // If you want to send messages to this application instance or
        // manage this apps subscriptions on the server side, send the
        // Instance ID token to your app server.
        sendRegistrationToServer(refreshedToken);

    }

    private void sendRegistrationToServer(String refreshToken) {
        String userId = Utils.getDefaults(Globals.PREFS_SAFERET_USRID, SafeRetApplication.getAppContext());
        if (userId == null) { //user not registered yet
            //store registration_id and send to server when user is registered for the firstTime
            Utils.setDefaults(PREFS_REG_TOKEN,refreshToken,SafeRetApplication.getAppContext());
        } else {

            updateRegistrationToken(refreshToken);
            Utils.setDefaults(PREFS_REG_TOKEN,refreshToken,SafeRetApplication.getAppContext());
        }

        System.out.println(refreshToken);
    }

    private String updateRegistrationToken(String refreshedToken) {

        UpdateTokenRequest updateTokenRequest = new UpdateTokenRequest(
                Utils.getDefaults(Globals.PREFS_SAFERET_USRID, SafeRetApplication.getAppContext()),
                Utils.getDefaults(Globals.PREFS_INST_MODE, SafeRetApplication.getAppContext()),
                refreshedToken
        );
        Call<String> call = apiService.updateRegistrationToken(updateTokenRequest);
        try {
            return call.execute().body();

        } catch (IOException e) {
            e.printStackTrace();
        }
        return Globals.FALSE;
    }


}
