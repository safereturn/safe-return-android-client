package safe.ret.android.service;

import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import java.util.Date;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import safe.ret.android.SafeRetApplication;
import safe.ret.android.dto.SafeReturnMessage;
import safe.ret.android.utils.Utils;

import static safe.ret.android.utils.Globals.PREFS_SAFERET_USRID;


public class SafeRetLocationListener implements LocationListener {

    private SafeReturnAPI apiService;

    public SafeRetLocationListener() {

        apiService = SafeReturnClient.getClient().create(SafeReturnAPI.class);
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onLocationChanged(Location location) {

        Log.d("onLocationChanged:", new Date().toString());
        Log.d("onLocationChanged:", "latitude" + String.valueOf(location.getLatitude()));
        Log.d("onLocationChanged:", "longitude:" + String.valueOf(location.getLongitude()));
        Utils.setDefaults("last_known_lat", String.valueOf(location.getLatitude()), SafeRetApplication.getAppContext());
        Utils.setDefaults("last_known_lng", String.valueOf(location.getLongitude()), SafeRetApplication.getAppContext());
        shareCurrentLocation(location);
    }

    @Override
    public void onProviderDisabled(String provider) {
        //TODO : Depending on best provider initialize appropriate intent;
        //	Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS );
        // 	startActivity(intent);
    }


    @Override
    public void onProviderEnabled(String provider) {
        Log.d("SafeRetLocationListener", provider + " is enabled");
    }

    private void shareCurrentLocation(android.location.Location location) {

        Call<SafeReturnMessage> call = apiService.checkLocation(getLocationDto(location));
         call.enqueue(new Callback<SafeReturnMessage>() {
            @Override
            public void onResponse(Call<SafeReturnMessage> call, Response<SafeReturnMessage> response) {
               SafeReturnMessage msg = response.body();
                Toast.makeText(SafeRetApplication.getAppContext(), msg.getMessage(), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<SafeReturnMessage> call, Throwable t) {
                Log.d("ERR", "ERR", t);
            }
        });
    }


    private safe.ret.android.dto.Location getLocationDto(android.location.Location location){
        safe.ret.android.dto.Location currentLocation = new safe.ret.android.dto.Location();
        currentLocation.setDate(new Date());
        currentLocation.setLongitude(location.getLongitude());
        currentLocation.setLatitude(location.getLatitude());
        currentLocation.setHome(false);
        currentLocation.setTrackedUserId(Utils.getDefaults(PREFS_SAFERET_USRID, SafeRetApplication.getAppContext()));
        return currentLocation;
    }
}
