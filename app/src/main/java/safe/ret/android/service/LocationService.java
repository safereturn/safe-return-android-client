package safe.ret.android.service;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.os.Binder;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.Toast;

import org.json.JSONObject;

import safe.ret.android.SafeRetApplication;
import safe.ret.android.utils.Utils;

/**
 * Created by Eva on 05-Feb-17.
 */

public class LocationService extends Service {


    private LocationManager locationManager;

    public LocationService() {
    }

    @Override
    public void onCreate() {

        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        Criteria criteria = new Criteria();
        String provider = locationManager.getBestProvider(criteria, false);
        Location location = locationManager.getLastKnownLocation(provider);

        if (location != null) {

            Utils.setDefaults("last_known_lat",String.valueOf( location.getLatitude()), SafeRetApplication.getAppContext());
            Utils.setDefaults("last_known_lng",String.valueOf( location.getLongitude()), SafeRetApplication.getAppContext());

            Log.d("LocationService", "Retrieved last known location: "+location.toString());
        }
       // minTime - minimum time interval between location updates, in milliseconds
       // minDistance - minimum distance between location updates, in meters
        locationManager.requestLocationUpdates(provider, 5, 1, new SafeRetLocationListener());
    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.i("LocalService", "Received start id " + startId + ": " + intent);
        return START_NOT_STICKY;
    }

    @Override
    public void onDestroy() {

        Log.d("LocationService", "onDestroy()");
        // Tell the user we stopped.
        Toast.makeText(this, "Stopped!", Toast.LENGTH_SHORT).show();
    }

    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    // This is the object that receives interactions from clients.  See
    // RemoteService for a more complete example.
    private final IBinder mBinder = new LocationService.LocalBinder();

    /**
     * Class for clients to access.  Because we know this service always
     * runs in the same process as its clients, we don't need to deal with
     * IPC.
     */
    public class LocalBinder extends Binder {
        LocationService getService() {
            return LocationService.this;
        }
    }
}