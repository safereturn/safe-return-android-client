package safe.ret.android.service;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Query;
import safe.ret.android.dto.AddContactRequest;
import safe.ret.android.dto.Contact;
import safe.ret.android.dto.Location;
import safe.ret.android.dto.OptionDto;
import safe.ret.android.dto.SafeReturnMessage;
import safe.ret.android.dto.UpdateTokenRequest;
import safe.ret.android.dto.UserDto;
import safe.ret.android.utils.Globals;

/**
 * Created by Eva on 18-Feb-17.
 */

public interface SafeReturnAPI {



    @PUT(Globals.REGISTER_TRACKED_USER_URL)
    Call<UserDto> createTrackedUser(@Body UserDto user);


    @PUT(Globals.REGISTER_CONTACT_USER_URL)
    Call<UserDto> createContactPerson(@Body UserDto user);


    @GET(Globals.GET_LAST_KNOWN_LOCATION_URL)
    Call<Location> getLastKnownLocation(@Query(Globals.TRACKED_USER_ID_PARAM) String trackedUserId);

    @GET(Globals.GET_LOCATIONS_URL)
    Call<List<Location>> getSafeLocations(@Query(Globals.TRACKED_USER_ID_PARAM) String trackedUserId,@Query(Globals.LOCATION_SAFE_PARAM) Boolean isSafe);

    @POST(Globals.PUT_LOCATION_URL)
    Call<Location> addSafeLocation(@Body Location location);



    @POST(Globals.UPDATE_TOKEN_URL)
    Call<String> updateRegistrationToken(@Body UpdateTokenRequest updateTokenRequest);

    @PUT(Globals.POST_OPTIONS_URL)
    Call<OptionDto> storeOptions(@Body OptionDto options);

    @GET(Globals.GET_CONTACTS_URL)
    Call<List<Contact>> getContacts(@Query(Globals.TRACKED_USER_ID_PARAM) String trackedUserId);

    @POST(Globals.CHECK_LOCATION_URL)
    Call<SafeReturnMessage> checkLocation(@Body Location location);


    @POST(Globals.ADD_CONTACT_URL)
    Call<SafeReturnMessage> addContact(@Body AddContactRequest addContactRequest);

}
