package safe.ret.android;

import android.app.Application;
import android.content.Context;

/**
 * Created by Eva on 06-Feb-17.
 */

public class SafeRetApplication extends Application {

    private static Context mContext;

    public void onCreate() {
        super.onCreate();
        mContext = getApplicationContext();
    }

    public static Context getAppContext() {
        return mContext;
    }

}