package safe.ret.android.listener;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.Marker;

import safe.ret.android.service.SafeReturnAPI;
import safe.ret.android.service.SafeReturnClient;

/**
 * Created by Eva on 21-Feb-17.
 */

public class RemoveMarkerListener implements GoogleMap.OnMarkerClickListener
{
    private Context mContext;
    private GoogleMap gMap;
    private SafeReturnAPI apiService;


    public RemoveMarkerListener(GoogleMap gmap, Context context) {
        this.gMap = gmap;
        this.mContext = context;
        this.apiService = SafeReturnClient.getClient().create(SafeReturnAPI.class);
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        final Marker toRemove = marker;
        new AlertDialog.Builder(mContext)
                .setTitle("Remove Location")
                .setMessage("Remove safe location?")
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                        System.out.println(toRemove.getPosition().longitude + " " + toRemove.getPosition().latitude);
                    }
                })
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // do nothing
                    }
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();

        return true;
    }
    }

