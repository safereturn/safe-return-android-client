
package safe.ret.android.listener;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import safe.ret.android.SafeRetApplication;
import safe.ret.android.dto.Location;
import safe.ret.android.service.SafeReturnAPI;
import safe.ret.android.service.SafeReturnClient;
import safe.ret.android.utils.Globals;
import safe.ret.android.utils.Utils;

import static safe.ret.android.utils.Globals.PREFS_SAFE_LOCATIONS_SET;
import static safe.ret.android.utils.Globals.PREFS_TRACKED_ID;


public class AddMarkerListener implements com.google.android.gms.maps.GoogleMap.OnMapClickListener {

    private Context mContext;
    private GoogleMap gMap;
    private SafeReturnAPI apiService;


    public AddMarkerListener(GoogleMap gmap, Context context) {
        this.gMap = gmap;
        this.mContext = context;
        this.apiService = SafeReturnClient.getClient().create(SafeReturnAPI.class);
    }

    @Override
    public void onMapClick(LatLng latLng) {
        final Marker safeLocation = gMap.addMarker(new MarkerOptions().position(latLng));
        new AlertDialog.Builder(mContext)
                .setTitle("Add Location")
                .setMessage("Add safe location?")
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        Location location = new Location();
                        location.setLatitude(safeLocation.getPosition().latitude);
                        location.setLongitude(safeLocation.getPosition().longitude);
                        location.setHome(true);
                        location.setDate(new Date());
                        location.setTrackedUserId(Utils.getDefaults(PREFS_TRACKED_ID, SafeRetApplication.getAppContext()));
                        addSafeLocation(location);
                    }
                })
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        safeLocation.remove();
                    }
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();

    }

    private void addSafeLocation(Location location) {

        Call<Location> call = apiService.addSafeLocation(location);
        call.enqueue(new Callback<Location>() {
            @Override
            public void onResponse(Call<Location> call, Response<Location> response) {
                Toast.makeText(SafeRetApplication.getAppContext(), "Safe location added!", Toast.LENGTH_SHORT).show();
                retrieveSafeLocations();
                Utils.setDefaults(PREFS_SAFE_LOCATIONS_SET, Globals.TRUE, SafeRetApplication.getAppContext());
            }

            @Override
            public void onFailure(Call<Location> call, Throwable t) {
                Log.d("ERR", "ERR", t);
            }
        });

    }

    private List<LatLng> retrieveSafeLocations() {

        final List<LatLng> locations = new ArrayList<>();
        Call<List<Location>> call = apiService.getSafeLocations(Utils.getDefaults(PREFS_TRACKED_ID, SafeRetApplication.getAppContext()),true);
        call.enqueue(new Callback<List<Location>>() {
            @Override
            public void onResponse(Call<List<Location>> call, Response<List<Location>> response) {
                List<Location> lst = response.body();
                if (lst != null) {
                    for (Location l : lst) {
                        locations.add(new LatLng(l.getLatitude(), l.getLongitude()));
                    }
                     addSafeLocationMarkers(locations);
                    addLastKnownkLocation();
                }
            }

            @Override
            public void onFailure(Call<List<Location>> call, Throwable t) {
                Log.d("ERR", "ERR", t);
            }
        });
        return locations;
    }


    private void addSafeLocationMarkers(List<LatLng> safeLocations) {
        gMap.clear();
        BitmapDescriptor MARKER_ICON = BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE);
        for (LatLng latlng : safeLocations) {
            gMap.addMarker(new MarkerOptions().icon(MARKER_ICON).position(latlng).title("Marker Title").snippet("Marker Description"));
        }
    }

    private void addLastKnownkLocation(){

        String lat = Utils.getDefaults("last_known_lat", SafeRetApplication.getAppContext());
        String lng = Utils.getDefaults("last_known_lng", SafeRetApplication.getAppContext());
        if (lat == null || lng ==null)
                return;
        LatLng   currentLoc = new LatLng(Double.valueOf(lat),Double.valueOf(lng));

        BitmapDescriptor MARKER_ICON = BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_VIOLET);
        gMap.addMarker(new MarkerOptions().icon(MARKER_ICON).position(currentLoc).title("Marker Title").snippet("Marker Description"));


    }

}


